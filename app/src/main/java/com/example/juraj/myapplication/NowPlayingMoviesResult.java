package com.example.juraj.myapplication;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juraj on 24-Feb-18.
 */

class NowPlayingMoviesResult {
    @SerializedName("results")
    List<ShowResult> mMovies;

    public NowPlayingMoviesResult(List<ShowResult> mMovies) {
        this.mMovies = mMovies;
    }

    public List<ShowResult> getmMovies() {
        return mMovies;
    }
    public void setMovies(List<ShowResult> movies) {
        mMovies = movies;
    }
}
