package com.example.juraj.myapplication;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Juraj on 17-Feb-18.
 */

public class ShowResult {
    private transient Long id;
    @SerializedName("id")
    private Integer mserverId;
    @SerializedName("title")
    private String mTtle;
    @SerializedName("poster_path")
    private String mPoster_path;
    @SerializedName("popularity")
    private String mPopularity;
    @SerializedName("overview")
    private String mOverview;

    public ShowResult() {
    }

    public ShowResult(Integer mserverId, String mTtle, String mPoster_path, String mPopularity, String mOverview) {
        this.mserverId = mserverId;
        this.mTtle = mTtle;
        this.mPoster_path = mPoster_path;
        this.mPopularity = mPopularity;
        this.mOverview = mOverview;
    }

    public ShowResult(Long id, Integer mserverId, String mTtle, String mPoster_path, String mPopularity, String mOverview) {
        this.id = id;
        this.mserverId = mserverId;
        this.mTtle = mTtle;
        this.mPoster_path = mPoster_path;
        this.mPopularity = mPopularity;
        this.mOverview = mOverview;
    }

    public Long getId() {
        return id;
    }

    public Integer getMserverId() {
        return mserverId;
    }

    public String getmTtle() {
        return mTtle;
    }

    public String getmPoster_path() {
        return TheMovieDatabaseAPI.BASE_IMAGE_URL + mPoster_path;
    }

    public String getPoster() {
        return mPoster_path;
    }

    public String getmPopularity() {
        return mPopularity;
    }

    public String getmOverview() {
        return mOverview;
    }
}
