package com.example.juraj.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.ibMovies)
    ImageView Movies;
    @BindView(R.id.ibSeries)
    ImageView Series;
    @BindView(R.id.ibActors)
    ImageView Actors;
    @BindView(R.id.bFavourites)
    ImageButton Favourites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ibMovies, R.id.ibSeries, R.id.ibActors, R.id.bFavourites})
    public void OnClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.ibMovies:
                intent.setClass(this, MoviesActivity.class);
                startActivity(intent);
                break;

            case R.id.bFavourites:
                intent.setClass(this, FavouritesActivity.class);
                startActivity(intent);
                break;
            case R.id.ibActors:
                intent.setClass(this,ActorListActivity.class);
                startActivity(intent);
                break;
        }
    }
}
