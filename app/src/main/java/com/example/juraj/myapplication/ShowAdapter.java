package com.example.juraj.myapplication;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juraj on 17-Feb-18.
 */

public class ShowAdapter extends BaseAdapter {
    List<ShowResult> mMovies;

    public ShowAdapter(List<ShowResult> mMovies) {
        this.mMovies = mMovies;
    }

    @Override
    public int getCount() {
        return this.mMovies.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mMovies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ShowViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_item_list, parent, false);
            holder = new ShowViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ShowViewHolder) convertView.getTag();
        }

        ShowResult show = this.mMovies.get(position);

        holder.tvShowTitle.setText(String.valueOf(show.getmTtle()));
        holder.tvShowPopularity.setText(show.getmPopularity());
        Picasso.with(parent.getContext())
                .load(show.getmPoster_path())
                .fit()
                .centerCrop()
//                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.background_color)
                .into(holder.ivShowPoster);

        return convertView;
    }
    static  class ShowViewHolder{
        @BindView(R.id.ivPoster)
        ImageView ivShowPoster;
        @BindView(R.id.tvTitle)
        TextView tvShowTitle;
        @BindView(R.id.tvPopularity)
        TextView tvShowPopularity;

        public ShowViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
}
