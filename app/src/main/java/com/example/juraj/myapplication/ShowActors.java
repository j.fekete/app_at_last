package com.example.juraj.myapplication;

import com.google.gson.annotations.SerializedName;

import retrofit2.http.GET;

/**
 * Created by Juraj on 28-Feb-18.
 */

class ShowActors {
    @SerializedName("id")
    Integer id;
    @SerializedName("name")
    String mName;
    @SerializedName("popularity")
    String mPopularity;
    @SerializedName("profile_path")
    String mPosterPath;
    @SerializedName("overview")
    String mOverview;

    public ShowActors(Integer id, String mName, String mPopularity, String mPosterPath, String mOverview) {
        this.id = id;
        this.mName = mName;
        this.mPopularity = mPopularity;
        this.mPosterPath = mPosterPath;
        this.mOverview = mOverview;
    }

    public Integer getId() {
        return id;
    }

    public String getmName() {
        return mName;
    }

    public String getmPopularity() {
        return mPopularity;
    }

    public String getmPosterPath() {
        return  TheMovieDatabaseAPI.BASE_IMAGE_URL + mPosterPath;
    }

    public String getmOverview() {
        return mOverview;
    }
}

