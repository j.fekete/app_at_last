package com.example.juraj.myapplication;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Juraj on 17-Feb-18.
 */

public interface TheMovieDatabaseAPI {
    String BASE_URL = "https://api.themoviedb.org/3/";
    String api_key = "";
    String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/w200";

    @GET("movie/popular")
    Call<PopularMoviesResult> getPopular(@Query("api_key") String key);

    @GET("movie/now_playing")
    Call<NowPlayingMoviesResult> getNowPlaying(@Query("api_key") String key);


    @GET("movie/top_rated")
    Call<TopRatedMoviesResult> getTopRatedMovies(@Query("api_key") String key);

    @GET("movie/upcoming")
    Call<UpcomingMoviesResult> getUpcomingMovies(@Query("api_key") String key);

    @GET("person/popular")
    Call<ActorsResult> getAllActors(@Query("api_key") String key);

    @GET("person/{id}")
    Call<ActorDetail> getActorDetail(@Path("id") int id, @Query("api_key") String key);
}
