package com.example.juraj.myapplication.utilis;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Movie;

import com.example.juraj.myapplication.ShowResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juraj on 28-Feb-18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final Integer DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "mojabaza";

    private static DatabaseHelper databaseHelper;

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        return databaseHelper;
    }

    private DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Tables
     */
    private static final String TABLE_MOVIES = "movies";

    /**
     * Keys
     */
    private static final String KEY_ID = "id";
    private static final String KEY_SERVER_ID = "serverId";
    private static final String KEY_TITLE = "title";
    private static final String KEY_POSTER_PATH = "poster_path";
    private static final String KEY_POPULARITY = "popularity";
    private static final String KEY_OVERVIEW = "overview";

    /**
     * Queries
     */
    private static final String QUERY_CREATE_MOVIES = "CREATE TABLE " + TABLE_MOVIES + " (" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_SERVER_ID + " INTEGER," +
            KEY_TITLE + " TEXT," +
            KEY_POSTER_PATH + " TEXT," +
            KEY_POPULARITY + " TEXT," +
            KEY_OVERVIEW + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(QUERY_CREATE_MOVIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVIES);
        onCreate(sqLiteDatabase);
    }

    public List<ShowResult> getAllMovies() {
        List<ShowResult> moviesList = new ArrayList<ShowResult>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MOVIES;

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                moviesList.add(new ShowResult(
                        Long.valueOf(cursor.getString(0)),
                        Integer.valueOf(cursor.getString(1)),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5)
                ));
            } while (cursor.moveToNext());
        }
        cursor.close();
        sqLiteDatabase.close();

        return moviesList;
    }

    public ShowResult getMovie(Integer id) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_MOVIES +
                " WHERE " + KEY_SERVER_ID + "=" + id;
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst()) {
            ShowResult movie = new ShowResult(
                    Long.valueOf(cursor.getString(0)),
                    Integer.valueOf(cursor.getString(1)),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5));

            cursor.close();
            sqLiteDatabase.close();

            return movie;
        } else {
            return null;
        }
    }

    public void createMovie(ShowResult movie) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, movie.getmTtle());
        values.put(KEY_SERVER_ID, movie.getMserverId());
        values.put(KEY_OVERVIEW, movie.getmOverview());
        values.put(KEY_POPULARITY, movie.getmPopularity());
        values.put(KEY_POSTER_PATH, movie.getPoster());

        sqLiteDatabase.insert(TABLE_MOVIES, null, values);
        sqLiteDatabase.close();
    }

    public void deleteMovie(ShowResult movie) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_MOVIES, KEY_ID + "=?",
                    new String[]{String.valueOf(movie.getId())});

        sqLiteDatabase.close();
    }
}
