package com.example.juraj.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoviesActivity extends Activity {


    @BindView(R.id.bNowPlaying)
    Button NowPlaying;
    @BindView(R.id.bPopular)
    Button Popular;
    @BindView(R.id.bTopRated)
    Button TopRated;
    @BindView(R.id.bUpcoming)
    Button Upcoming;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bNowPlaying, R.id.bPopular, R.id.bTopRated, R.id.bUpcoming})
    public void OnClick(View view){
        Intent intent = new Intent();
        switch (view.getId()){
            case R.id.bPopular:
                intent.setClass(this,PopularMoviesActivity.class);
                startActivity(intent);
                break;
            case R.id.bNowPlaying:
                intent.setClass(this,NowPlayingActivity.class);
                startActivity(intent);
                break;
            case R.id.bTopRated:
                intent.setClass(this,TopRatedMoviesActivity.class);
                startActivity(intent);
                break;

            case R.id.bUpcoming:
                intent.setClass(this,UpcomingMoviesActivity.class);
                startActivity(intent);
                break;
        }
    }


}
