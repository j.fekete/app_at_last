package com.example.juraj.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Movie;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.juraj.myapplication.utilis.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouritesActivity extends Activity implements AdapterView.OnItemLongClickListener,AdapterView.OnItemClickListener {

    private DatabaseHelper databaseHelper;
    private List<ShowResult> showList;
    @BindView(R.id.lvFavourites)
    GridView lvFavourites;
    ShowAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        ButterKnife.bind(this);

        lvFavourites.setOnItemLongClickListener(this);
        lvFavourites.setOnItemClickListener(this);
        databaseHelper = DatabaseHelper.getInstance(this);

        showList = databaseHelper.getAllMovies();
        adapter = new ShowAdapter(showList);
        lvFavourites.setAdapter(adapter);
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

        final ShowResult clickedResult = showList.remove((int) l);
        databaseHelper.deleteMovie(clickedResult);
        adapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ShowResult clickedResult = showList.get((int) l);

        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_movie, null);
        ImageView dialogImageViewMovie = dialogView.findViewById(R.id.ivDialogImage);
        TextView dialogTextViewMovie = dialogView.findViewById(R.id.tvDialogOverview);

        Picasso.with(this)
                .load(clickedResult.getmPoster_path())
                .fit()
                .centerCrop()
//                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.background_color)
                .into(dialogImageViewMovie);
        dialogTextViewMovie.setText(clickedResult.getmOverview());

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setView(dialogView).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}

