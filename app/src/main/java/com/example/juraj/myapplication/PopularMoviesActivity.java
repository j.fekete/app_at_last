package com.example.juraj.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.juraj.myapplication.utilis.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class PopularMoviesActivity extends Activity implements Callback<PopularMoviesResult>, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private static final String ERROR_TAG = "Juraj";
    @BindView(R.id.lvPopularMovies)
    GridView lvPopularMovies;
    private DatabaseHelper databaseHelper;

    private List<ShowResult> showList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popular_movies);
        ButterKnife.bind(this);
        this.setUpRetrofit();
        lvPopularMovies.setOnItemClickListener(this);
        databaseHelper = DatabaseHelper.getInstance(this);
        lvPopularMovies.setOnItemLongClickListener(this);
    }

    private void setUpRetrofit() { Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(TheMovieDatabaseAPI.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient())
            .build();
        TheMovieDatabaseAPI Movieapi = retrofit.create(TheMovieDatabaseAPI.class);
        Call<PopularMoviesResult> request = Movieapi.getPopular(TheMovieDatabaseAPI.api_key);
        request.enqueue(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ShowResult clickedResult = showList.get((int) l);

        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_movie, null);
        ImageView dialogImageViewMovie = dialogView.findViewById(R.id.ivDialogImage);
        TextView dialogTextViewMovie = dialogView.findViewById(R.id.tvDialogOverview);

        Picasso.with(this)
                .load(clickedResult.getmPoster_path())
                .fit()
                .centerCrop()
//                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.background_color)
                .into(dialogImageViewMovie);
        dialogTextViewMovie.setText(clickedResult.getmOverview());

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setView(dialogView).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void onResponse(Call<PopularMoviesResult> call, Response<PopularMoviesResult> response) {
        showList = response.body().getMovies();
        ShowAdapter adapter = new ShowAdapter(showList);
        lvPopularMovies.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<PopularMoviesResult> call, Throwable t) {
        Log.e(ERROR_TAG, t.getMessage());
        Toast.makeText(this, R.string.app_name, Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        final ShowResult clickedResult = showList.get((int) l);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_favourite, null);
        ImageView dialogImageViewMovie = dialogView.findViewById(R.id.ivDialogPoster);

        Picasso.with(this)
                .load(clickedResult.getmPoster_path())
                .fit()
                .centerCrop()
//                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.background_color)
                .into(dialogImageViewMovie);

        ShowResult showResult = databaseHelper.getMovie(clickedResult.getMserverId());
        builder.setView(dialogView);
        if(showResult == null) {
            builder.setPositiveButton(getResources().getString(R.string.show_favoruites), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    databaseHelper.createMovie(clickedResult);
                    dialog.dismiss();
                }
            });
        }

        AlertDialog dialog = builder.create();
        dialog.show();
        return true;
    }
}