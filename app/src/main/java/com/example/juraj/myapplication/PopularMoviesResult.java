package com.example.juraj.myapplication;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juraj on 17-Feb-18.
 */

class PopularMoviesResult {
    @SerializedName("results")
    List<ShowResult> mMovies;

    public PopularMoviesResult() {
    }

    public PopularMoviesResult(List<ShowResult> movies) {
        mMovies = movies;
    }

    public List<ShowResult> getMovies() {
        return mMovies;
    }

    public void setMovies(List<ShowResult> movies) {
        mMovies = movies;
    }
}
