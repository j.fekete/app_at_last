package com.example.juraj.myapplication;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juraj on 25-Feb-18.
 */

class TopRatedMoviesResult {
    @SerializedName("results")
    List<ShowResult> mMovies;

    public TopRatedMoviesResult(List<ShowResult> mMovies) {
        this.mMovies = mMovies;
    }

    public List<ShowResult> getmMovies() {
        return mMovies;
    }
    public void setMovies(List<ShowResult> movies) {
        mMovies = movies;
    }
}
