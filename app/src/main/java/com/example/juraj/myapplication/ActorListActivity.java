package com.example.juraj.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.juraj.myapplication.utilis.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ActorListActivity extends Activity implements Callback<ActorsResult>, AdapterView.OnItemClickListener {
    private static final String ERROR_TAG = "Juraj";
    @BindView(R.id.lvActors)
    GridView lvActors;
    private DatabaseHelper databaseHelper;

    private List<ShowActors> showActors;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_list);
        ButterKnife.bind(this);
        this.setUpRetrofit();

        lvActors.setOnItemClickListener(this);
        databaseHelper = DatabaseHelper.getInstance(this);
    }

    private void setUpRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TheMovieDatabaseAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();
        TheMovieDatabaseAPI Movieapi = retrofit.create(TheMovieDatabaseAPI.class);
        Call<ActorsResult> request = Movieapi.getAllActors(TheMovieDatabaseAPI.api_key);
        request.enqueue(this);
    }

    @Override
    public void onResponse(Call<ActorsResult> call, Response<ActorsResult> response) {
        showActors = response.body().getmActors();
        ActorsAdapter adapter = new ActorsAdapter(showActors);
        lvActors.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<ActorsResult> call, Throwable t) {
        Log.e(ERROR_TAG, t.getMessage());
        Toast.makeText(this, R.string.app_name, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.e("CLICKED", showActors.get((int) l).getmName());

        this.getPersonDetails((int)l);

    }

    private void getPersonDetails(final Integer id) {
        final ShowActors clickedResult = showActors.get(id);
        Log.e("Actor id:", String.valueOf(clickedResult.getId()));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TheMovieDatabaseAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();
        TheMovieDatabaseAPI Movieapi = retrofit.create(TheMovieDatabaseAPI.class);
        Call<ActorDetail> request = Movieapi.getActorDetail(clickedResult.getId(), TheMovieDatabaseAPI.api_key);
        request.enqueue(new Callback<ActorDetail>() {
            @Override
            public void onResponse(Call<ActorDetail> call, Response<ActorDetail> response) {
                Log.e("Actor details:", String.valueOf(response));

                ActorDetail actorDetail = response.body();

                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(ActorListActivity.this);
                LayoutInflater inflater = ActorListActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_movie, null);
                ImageView dialogImageViewMovie = dialogView.findViewById(R.id.ivDialogImage);
                TextView dialogTextViewMovie = dialogView.findViewById(R.id.tvDialogOverview);

                Picasso.with(ActorListActivity.this)
                        .load(clickedResult.getmPosterPath())
                        .fit()
                        .centerCrop()
//                .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.background_color)
                        .into(dialogImageViewMovie);
                try {
                    dialogTextViewMovie.setText(actorDetail.getmBiography());
                } catch (NullPointerException e) {
                    Log.e("Error", "NullpointerExp actor");
                }

                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setView(dialogView).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

                // 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();

            }

            @Override
            public void onFailure(Call<ActorDetail> call, Throwable t) {
                Log.e(ERROR_TAG, t.getMessage());
                Toast.makeText(ActorListActivity.this, R.string.app_name, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
