package com.example.juraj.myapplication;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juraj on 28-Feb-18.
 */

class ActorsResult {
    @SerializedName("results")
    List<ShowActors> mActors;

        public ActorsResult(){}

    public ActorsResult(List<ShowActors> actors) {
        this.mActors = actors;
    }

    public List<ShowActors> getmActors() {
        return mActors;
    }

    public void setmActors(List<ShowActors> mActors) {
        this.mActors = mActors;
    }
}