package com.example.juraj.myapplication;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Juraj on 08-Mar-18.
 */

public class ActorDetail {
    @SerializedName("id")
    Integer id;
    @SerializedName("name")
    String mName;
    @SerializedName("popularity")
    String mPopularity;
    @SerializedName("profile_path")
    String mPosterPath;
    @SerializedName("biography")
    String mBiography;

    public ActorDetail(Integer id, String mName, String mPopularity, String mPosterPath, String mBiography) {
        this.id = id;
        this.mName = mName;
        this.mPopularity = mPopularity;
        this.mPosterPath = mPosterPath;
        this.mBiography = mBiography;
    }

    public Integer getId() {
        return id;
    }

    public String getmName() {
        return mName;
    }

    public String getmPopularity() {
        return mPopularity;
    }

    public String getmPosterPath() {
        return  TheMovieDatabaseAPI.BASE_IMAGE_URL + mPosterPath;
    }

    public String getmBiography() {
        return mBiography;
    }
}
