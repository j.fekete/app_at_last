package com.example.juraj.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juraj on 28-Feb-18.
 */

public class ActorsAdapter extends BaseAdapter {
    List<ShowActors> mActors;

    public ActorsAdapter(List<ShowActors> mActors) {
        this.mActors = mActors;
    }

    @Override
    public int getCount() {
        return this.mActors.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mActors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ActorsAdapter.ShowViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_item_list, parent, false);
            holder = new ShowViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ActorsAdapter.ShowViewHolder) convertView.getTag();
        }
        ShowActors show = this.mActors.get(position);
        holder.tvShowTitle.setText(String.valueOf(show.getmName()));
        holder.tvShowPopularity.setText(show.getmPopularity());
        Picasso.with(parent.getContext())
                .load(show.getmPosterPath())
                .fit()
                .centerCrop()
//                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.background_color)
                .into(holder.ivShowPoster);

        return convertView;
    }





    static  class ShowViewHolder{
        @BindView(R.id.ivPoster)
        ImageView ivShowPoster;
        @BindView(R.id.tvTitle)
        TextView tvShowTitle;
        @BindView(R.id.tvPopularity)
        TextView tvShowPopularity;

        public ShowViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }

}
